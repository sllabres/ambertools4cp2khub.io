---
title: Preparing your PDB file
teaching: 15
exercises: 0
questions:
- "What should I check before preparing a simulation?"
objectives:
- "Read the literature on your target protein"
- "Assess protonation states for your system with `propka`."
- "Prepare your protein with `pdb4amber`."
keypoints:
- "`pdb4amber` does a preliminary check on your PDB file and cleans potential errors in the protein structure."
- "You should ALWAYS check the protonation state of your biological system."
---

## Understanding your PDB file

We are going to use the X-ray structure of Mpro protein bound to a ligand from the PanDDA library. The PDB id we are going to use is [5R84](http://www.rcsb.org/structure/5R84). If you open this entry from the [Protein DataBank](https://www.rcsb.org) you will find a lot of information of the protein structure:
- Experimental Data Snapshot
- wwPDB Validation
- Literature
- Macromolecules
- Small Molecules
- Experimental Data & Validation
- Entry History 

![Snapshot of PDB page 5R84]({{ page.root }}/fig/PDBpage.png)

We are interested in the quality of the structure and the contents of the structure:
- **Experimental Data Snapshot** and **wwPDB Validation** give us information about the quality of the model. In this case, the X-ray structure is not so great but this fragment screening was resolved really fast to enable computational efforts. So it will have to do. For more information you can check out Bioexcel webinar on [Assessing structure quality in the PDB archive](https://bioexcel.eu/webinar-10-assessing-structure-quality-in-the-pdb-archive/). 
- The sections **Macromolecules** and **Small Molecules** give us information about the contents of the PDB file. We can see that there is only one macromolecule (only one chain) that corresponds to SARS-CoV-2 main protease. There are two small molecules in the structure: GWS and DMS. 
   - DMS is a crystallisation product and it doesn't interest us. 
   - GWS is the fragment we want to model.  

Once we know which molecules in the PDB file we want to model and which should be deleted from the PDB file, we can continue. 
You can download the PDB file now or use the one we are providing in this repository. 

***

## Splitting the PDB in protein, ligand and water molecules

First, we will split the structure in the PDB in 3 different groups: protein, GWS ligand and water molecules as we need to treat them separately: 

~~~
grep -v -e "GWS" -e "DMS" -e "CONECT" -e "HOH" 5r84.pdb >  protein.pdb
grep "GWS" 5r84.pdb > GWS.pdb
grep "HOH" 5r84.pdb > waters.pdb
~~~
{: .language-bash}

![PDB contents split in three groups]({{ page.root }}/fig/fullsystem.png)

***

## Replicating the experimental/target conditions

It is important to take into account experimental conditions of the system we want to simulate such as the pH, salinity… Since Mpro protein from SARS-CoV2 is located in the cytosol of the infected human cell so we are going to assume a **pH=7.4**. Please check the literature on your target protein before you start running your simulations.

As we mentioned before, we will have two molecules: the Mpro protein and the GWS ligand. We will have to take care of the effect of pH in each one of them. 

***

### Assessing the protonation states of the protein and preparing the protein for AMBERTools

There are several ways to assess the protonation of the residues in the protein ([H++ server](http://biophysics.cs.vt.edu), [propka](http://propka.org)). We are going to use propKa command-line in this tutorial:

~~~
propka31 protein.pdb 
~~~
{: .language-bash}

PropKa returns the calculated pKa values for each residue. We have to compare each one of the pKa values with the chosen pH: if the pKa is below the chosen pH the residue should be deprotonated and above the chosen pH it will be protonated. PropKa returns the model-pKa for each kind of residue, which determines the *usual* protonation state for that type of residue. 

> ## TIP:
>
> This step is not trivial when you are simulating catalytic sites, where the protonation changes can be part of the reaction mechanism. Please check the literature on your target protein before you start to run your simulations.
{: .callout}

This is the summary of the propKa output for our protein:

~~~
--------------------------------------------------------------------------------------------------------
SUMMARY OF THIS PREDICTION
       Group      pKa  model-pKa   ligand atom-type
   ASP  33 A     3.79       3.80                      
   ASP  34 A     3.63       3.80                      
   ASP  48 A     2.69       3.80                      
   ASP  56 A     4.00       3.80                      
   ASP  92 A     3.11       3.80                      
   ASP 153 A     3.88       3.80                      
   ASP 155 A     3.00       3.80                      
   ASP 176 A     3.90       3.80                      
   ASP 187 A     3.97       3.80                      
   ASP 197 A     3.81       3.80                      
   ASP 216 A     3.52       3.80                      
   ASP 229 A     2.15       3.80                      
   ASP 245 A     4.03       3.80                      
   ASP 248 A     3.37       3.80                      
   ASP 263 A     3.53       3.80                      
   ASP 289 A     3.28       3.80                      
   ASP 295 A     3.91       3.80                      
   GLU  14 A     4.00       4.50                      
   GLU  47 A     4.67       4.50                      
   GLU  55 A     4.73       4.50                      
   GLU 166 A     3.98       4.50                      
   GLU 178 A     4.88       4.50                      
   GLU 240 A     4.60       4.50                      
   GLU 270 A     4.67       4.50                      
   GLU 288 A     4.42       4.50                      
   GLU 290 A     5.79       4.50                      
   HIS  41 A     4.60       6.50                      
   HIS  64 A     6.26       6.50                      
   HIS  80 A     5.71       6.50                      
   HIS 163 A     2.04       6.50                      
   HIS 164 A     1.44       6.50                      
   HIS 172 A     5.51       6.50                      
   HIS 246 A     5.37       6.50                      
   CYS  16 A    11.92       9.00                      
   CYS  22 A    10.26       9.00                      
   CYS  38 A    12.83       9.00                      
   CYS  44 A    11.04       9.00                      
   CYS  85 A    11.68       9.00                      
   CYS 117 A    11.97       9.00                      
   CYS 128 A    12.86       9.00                      
   CYS 145 A    11.82       9.00                      
   CYS 156 A     9.54       9.00                      
   CYS 160 A    13.16       9.00                      
   CYS 265 A    12.18       9.00                      
   CYS 300 A    10.29       9.00                      
   TYR  37 A    11.88      10.00                      
   TYR  54 A    15.15      10.00                      
   TYR 101 A    12.87      10.00                      
   TYR 118 A    10.28      10.00                      
   TYR 126 A    13.10      10.00                      
   TYR 154 A    10.17      10.00                      
   TYR 161 A    15.42      10.00                      
   TYR 182 A    13.69      10.00                      
   TYR 209 A    13.21      10.00                      
   TYR 237 A    10.15      10.00                      
   TYR 239 A    13.26      10.00                      
   LYS   5 A    10.57      10.50                      
   LYS  12 A    10.41      10.50                      
   LYS  61 A    10.72      10.50                      
   LYS  88 A    10.14      10.50                      
   LYS  90 A    10.56      10.50                      
   LYS  97 A    10.37      10.50                      
   LYS 100 A    11.37      10.50                      
   LYS 102 A    10.76      10.50                      
   LYS 137 A    10.33      10.50                      
   LYS 236 A    10.69      10.50                      
   LYS 269 A    11.49      10.50                      
   ARG   4 A    12.41      12.50                      
   ARG  40 A    14.21      12.50                      
   ARG  60 A    12.46      12.50                      
   ARG  76 A    12.59      12.50                      
   ARG 105 A    13.13      12.50                      
   ARG 131 A    15.69      12.50                      
   ARG 188 A    12.33      12.50                      
   ARG 217 A    12.15      12.50                      
   ARG 222 A    12.45      12.50                      
   ARG 279 A    12.06      12.50                      
   ARG 298 A    12.61      12.50                      
   N+    1 A     7.91       8.00  
~~~
{: .output}

We can see that all the residues are in their usual protonation state, therefore we do not need to modify residue names in the PDB. If you perform this step using the [H++ server](http://biophysics.cs.vt.edu), it will give you the same information and also a new PDB file with the necessary changes applied. 

> ## TIP:
>
> **Histidines** are tricky residues, they have three possible protonation states: HID, HIE and HIP. HIP is the protonated residue. HID and HIE correspond to the neutral histidine protonated in *delta* or *epsilon* position. In solution, the most common conformer is HIE but always visualise your structure before assuming any histidine protonation state. 
>
>![H163 stablishes an H-bond interaction with GWS ligand.]({{ page.root }}/fig/H163.png)
{: .callout}

Last but not least, we need to clean the PDB file before feeding it to AmberTools. So we are going to use the first AmberTools tool: ```pdb4amber``` which cleans/prepares your PDB file. 

~~~
pdb4amber -i protein.pdb -o protein4amber.pdb  
~~~
{: .language-bash}

It returns the following output:

~~~
==================================================
Summary of pdb4amber for: protein.pdb
===================================================

----------Chains
The following (original) chains have been found:
A

---------- Alternate Locations (Original Residues!))

The following residues had alternate locations:
VAL_73
ARG_217
ASN_221
-----------Non-standard-resnames


---------- Mising heavy atom(s)

None
The alternate coordinates have been discarded.
Only the first occurrence for each atom was kept.
~~~
{: .output}

```pdb4amber``` informs us about the duplicity of several sidechains, disulphide bonds and missing heavy atoms. It does not check protonation states for protein residues and therefore we **ALWAYS** must run a pKa calculation.  

*** 

### Assessing the protonation states of the ligand

We have check the protonation state of the protein but we have omited the ligand. We have to check the pKa of the ligand. There are several options to do so, the most reliable one if available is [PubChem](https://pubchem.ncbi.nlm.nih.gov), where you can find experimental pKa values. 

In this example, there are no pKa values (nor experimental nor calculated) reported for this molecule, so we need to use another resource. I like [Marvin Sketch](https://chemaxon.com/products/marvin): it is *free* to use, gives calculated pKa values and a nice visualisation of the chemical species at a certain pH.  

![pKa calculation of the GWS ligand]({{ page.root }}/fig/pKa_gws.png)

After assessing the protonation state of the GWS ligand using Marvin Sketch, we can proceed to protonate it. The easiest way to protonate it is using [OpenBabel](http://openbabel.org).

~~~
obabel -ipdb GWS.pdb -opdb -O GWS.H.pdb -h 
~~~
{: .language-bash}

You should always check the result of this step and ammend it if needed. In this case, it gives the correct protonation state:

![protonated GWS ligand]({{ page.root }}/fig/GWS.png)

