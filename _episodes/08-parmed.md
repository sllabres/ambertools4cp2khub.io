---
title: Amend the forcefield before running QM/MM runs 
teaching: 10
exercises: 0
questions:
- "Do I have all the necessary parameters to run a QM/MM simulation?"
objectives:
- "Amend missing parameters in the AMBER forcefields."
- "Using `parmed` to change parameters in AMBER7 topologies."
keypoints:
- "Adding missing Lennard-Jones parameters to the topology will prevent having unphysical interactions between the QM and MM subsystems."
---

We need to amend the force field before starting QM/MM calculations because the amber forcefiled does not have Lennard-Jones parameters for all the hydrogen atoms (in particular TIP3P water models and hydroxyl groups from seriens and tyrosines). We need to correct the topology with the tool ```parmed```. 

~~~
parmed system.parm7
printDetails @4685
changeLJSingleType :WAT@H1 0.3019 0.047
changeLJSingleType :WAT@H2 0.3019 0.047
changeLJSingleType :SER@HG 0.3019 0.047
changeLJSingleType :TYR@HH 0.3019 0.047
outparm system_LJ_mod.parm7
quit
~~~
{: .language-bash}

This step prevents creating unphysical interactions between the hydrogen atoms of water and serine and QM subsystem. After topology correction the thermostat should stay stable during the QM/MM calculation. 

cpptraj to fix format of the rst7 file:

~~~
cpptraj -p systemx_LJ_mod.parm7
trajin system.equil.rst7
autoimage
trajout system.equil0.rst7 inpcrd
go 
quit
~~~
{: .language-bash}
