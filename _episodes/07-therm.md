---
title: Thermalisation and Density equilibration
teaching: 15
exercises: 0
questions:
- "How do I equilibrate my system with `sander`?"
objectives:
- "Explain why thermalisation and density equilibration are important."
- "Use `sander` to perform thermalisation (NVT) and density equilibration (NPT)."
keypoints:
- "Thermalisation takes our system up to the correct temperature."
- "Density equilibration fixes the simulation box and prevents air bubbles in our system."
---

After the minimisation step, we will heat the system up to the target temperature 310K (36°C) at constant volume and then after equilibrate the pressure and density of the system at contant pressure.  

## Thermalisation

We will use the input file **sander_heat.in** to thermalisation, where we use a temperature ramp that will slowly increase the temperature and allow to allow the system to accomodate.

~~~
Heating ramp from 100K to 300K 
 &cntrl
  imin=0,                   ! Run molecular dynamics.
  ntx=1,                    ! Initial file contains coordinates, but no velocities.
  irest=0,                  ! Do not restart the simulation
  nstlim=10000,             ! Number of MD-steps to be performed.
  dt=0.002,                 ! Time step (ps)
  ntf=2, ntc=2,             ! Constrain lengths of bonds having hydrogen atoms (SHAKE)
  tempi=0.0, temp0=300.0,   ! Initial and final temperature
  ntpr=100, ntwx=100,       ! Output options
  cut=8.0,                  ! non-bond cut off
  ntb=1,                    ! Periodic conditiond at constant volume
  ntp=0,                    ! No pressure scaling
  ntt=3, gamma_ln=2.0,      ! Temperature scaling using Langevin dynamics with the collision frequency in gamma_ln (ps−1)
  ig=-1,                    ! seed for the pseudo-random number generator will be based on the current date and time.
  nmropt=1,                 ! NMR options to give the temperature ramp.
 /
&wt type='TEMP0', istep1=0, istep2=9000, value1=0.0, value2=300.0 /
&wt type='TEMP0', istep1=9001, istep2=10000, value1=300.0, value2=300.0 /
&wt type='END' /
~~~
{: .source}

The final results are here: **TO UPDATE**

~~~
 NSTEP =    10000   TIME(PS) =      20.000  TEMP(K) =   298.87  PRESS =     0.0
 Etot   =   -156391.2390  EKtot   =     39292.6561  EPtot      =   -195683.8951
 BOND   =       949.1198  ANGLE   =      2397.4235  DIHED      =      3895.6936
 1-4 NB =      1090.0599  1-4 EEL =     12054.3523  VDWAALS    =     24789.0486
 EELEC  =   -240859.5929  EHBOND  =         0.0000  RESTRAINT  =         0.0000
 Ewald error estimate:   0.3083E-04
~~~
{: .output}

***

## Pressure equilibration

Then, we will use the input file **sander_equil.in** to equilibrate the density of the system:

~~~
Density equilibration
&cntrl
  imin= 0,                       ! Run molecular dynamics.
  nstlim=25000,                  ! Number of MD-steps to be performed.
  dt=0.002,                      ! Time step (ps)
  irest=1,                       ! Restart the simulation
  ntx=5,                         ! Initial file contains coordinates and velocities.
  ntpr=100, ntwx=100, ntwr=100,  ! Output options
  cut=8.0,                       ! non-bond cut off
  temp0=298,                     ! Temperature
  ntt=3, gamma_ln=3.0,           ! Temperature scaling using Langevin dynamics with the collision frequency in gamma_ln (ps−1)
  ntb=2,                         ! Periodic conditiond at constant pressure
  ntc=2, ntf=2,                  ! Constrain lengths of bonds having hydrogen atoms (SHAKE)
  ntp=1, taup=2.0,               ! Pressure scaling
  iwrap=1, ioutfm=1,             ! Output trajectory options
/
~~~
{: .source}

The final results are here: **TO UPDATE**

~~~
 NSTEP =    10000   TIME(PS) =      20.000  TEMP(K) =   298.87  PRESS =     0.0
 Etot   =   -156391.2390  EKtot   =     39292.6561  EPtot      =   -195683.8951
 BOND   =       949.1198  ANGLE   =      2397.4235  DIHED      =      3895.6936
 1-4 NB =      1090.0599  1-4 EEL =     12054.3523  VDWAALS    =     24789.0486
 EELEC  =   -240859.5929  EHBOND  =         0.0000  RESTRAINT  =         0.0000
 Ewald error estimate:   0.3083E-04
~~~
{: .output}
