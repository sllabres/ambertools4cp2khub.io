---
title: Overview of AmberTools20
teaching: 5
exercises: 0
questions:
- "What is AmberTools?"
objectives:
- "Overview of AmberTools suite."
- "Understand what can AmberTools do for you."
keypoints:
- "AmberTools suite overview has multiple tools to simulate biological molecules."
- "```tleap``` is used to generate topologies and coordinates for biological molecules."
- "```cpptraj``` is used to postprocess AMBER trajectories."
- "```antechamber``` is used to create parameters for non-typical molecules."
- "```sander``` runs molecular dynamics simulations"
- "```parmed``` allows modifying AMBER topologies"
---

[AmberTools suite](https://ambermd.org/AmberTools.php) is a set of several independently developed packages developed as part of the Amber20 software package and distributed for free of charge, and its components are mostly released under the GNU General Public License (GPL). They work well by themselves, and with Amber20 itself.

**Overview of AmberTools20:** (released on April 31, 2020)

- **NAB/sff**: a program build molecules, run MD or apply distance geometry restraints using generalized Born, Poisson-Boltzmann or 3D-RISM implicit solvent models
- **antechamber and MCPB**: programs to create force fields for general organic molecules and metal centers
- **tleap and parmed**: basic preparatory tools for Amber simulations
- **sqm**: semiempirical and DFTB quantum chemistry program
- **pbsa**: performs numerical solutions to Poisson-Boltzmann models
- **3D-RISM**: solves integral equation models for solvation
- **sander**: workhorse program for molecular dynamics simulations
- **gem.pmemd**: tools for using advanced force fields
- **mdgx**: a program for pushing the boundaries of Amber MD, primarily through parameter fitting. Also includes customizable virtual sites and explicit solvent MD capabilities.
- **cpptraj and pytraj**: tools for analyzing structure and dynamics in trajectories
- **MMPBSA.py**: energy-based analyses of MD trajectories

We are going to use a subset of these tools: **antechamber (sqm), tleap, parmed, sander and cpptraj**.

