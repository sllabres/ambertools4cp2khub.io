---
title: Log in and environment set up in ARCHER 
teaching: 5
exercises: 0
questions:
- "How do I set up the correct environment in ARCHER?"
objectives:
- "Log in in ARCHER."
- "Load the correct envinronment in ARCHER."
keypoints:
-   "ARCHER usage for Ambertools with `module load`."
---

Accessing ARCHER:

~~~
ssh -XY user@login.archer.ac.uk
~~~
{: .language-bash}

**Software we will be using in this session:**
- [AmberTools](https://ambermd.org/AmberTools.php) (We will use the latest version of Ambertools. If you are using an older version, commands might run slightly different.)
- [propKa](http://propka.org)
- [OpenBabel](http://openbabel.org/wiki/Main_Page)
- [Pymol](https://sourceforge.net/projects/pymol/) or [VMD](https://www.ks.uiuc.edu/Research/vmd/) or another molecular visualisation tool of your preference. 
- [Marvin Sketch](https://chemaxon.com/products/marvin). 

To set up the proper environment you should run the following commands on ARCHER:

~~~
module load propka
module load amber-tools/20
module load openbabel
~~~~
{: .language-bash}


