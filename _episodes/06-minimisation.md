---
title: Energy minimisation
teaching: 10
exercises: 0
questions:
- "How can I refine and remove bad contacts from the initial structure?" 
objectives:
- "Run a `sander` command to minimise the initial structure."
- "Explain briefly the option in the sander input file."
keypoints:
- "Removing bad contacts at this point will prevent our system from blowing up later on."
--- 

Before running any QM/MM simulation, we must **ALWAYS** equilibrate the system using only molecular mechanics (MM). To do so we must minimise the structure first to avoid bad contacts that might blow-up our simulation. 

These steps might take some time, therefore we are going to submit the calculation NOW on the short queue of ARCHER using the following script:

~~~
sbatch send_mm_equil.pbs
~~~
{: .language-bash}

First, we are going to minimise and equilibrate the system using ```sander``` tool from AmberTools suite. We have provided commented input files and ARCHER submission scripts to make these steps easier to follow. You can find more information on the sander input options in [Chapter 19](https://ambermd.org/doc12/Amber20.pdf) of the Amber20 manual.

```sander``` needs the initial coordinates (```-c system.rst7```), topology (```-p system.parm7```) and input file (```-i sander_min.in```). Also, we can specify the name of output files such as output file (```-o```) and final coordinates (```-r```). 

~~~
sander -O -i sander_min.in -o min_classical.out -p system.parm7 -c system.rst7 -r system.min.rst7
~~~
{: .language-bash}

We will minimise in 4000 steps using two different methods: 2000 of steepest descent and 2000 of conjugate gradient. The sander input file looks like this:

~~~
Minimisation of system 
 &cntrl
  imin=1,        ! Perform an energy minimization.
  maxcyc=4000,   ! The maximum number of cycles of minimization. 
  ncyc=2000,     ! The method will be switched from steepest descent to conjugate gradient after NCYC cycles.
 /
~~~
{: .source}

The final results are here: 

~~~
   NSTEP       ENERGY          RMS            GMAX         NAME    NUMBER
   4000      -2.6417E+05     1.7682E-01     1.6648E+01     C        4677

 BOND    =    20078.9768  ANGLE   =      604.5946  DIHED      =     3420.4551
 VDWAALS =    48544.6347  EEL     =  -349847.2692  HBOND      =        0.0000
 1-4 VDW =      954.3963  1-4 EEL =    12077.9836  RESTRAINT  =        0.0000
~~~
{: .output}



