---
title: System set up 
teaching: 10
exercises: 0
questions:
- "How to create topologies and coordinates using Leap?"
objectives:
- "Create topologies with `tleap`."
keypoints:
- "Leap creates topologies and initial coordinates in AMBER7 format."
- "`xleap` and `tleap` are the two flavours of Leap."
- "**ALWAYS check the structures created by Leap!**"
---

Once we have parameters for our ligand and a prepared protein PDB file, we can start building our system and creating topology and coordinate files for AMBER. To create the mentioned files we will use ```Leap```. there are two versions of leap: the GUI version ```xLeap``` and the terminal version ```tLeap```. ```xLeap``` is better to learn Leap because you can visualise the result of each command you try. However, ```xleap``` is quite buggy and it can be frustrating before you get used to all its quirks. 

In this session, we are going to use ```tleap``` and I am going to walk you through every command we use. If you type tleap you should enter in the tleap command-line. If you want to check what you did in previous session of Leap, Leap is quite verbose and informs you of everything it does as well writing it into the **leap.log** file. 

We have prepared a leap script to set up our system and we are going to explain briefly.  

We are going to use default force fields (amberff14SB, tip3p and GAFF2) in this tutorial, so we can use the leaprc scripts already included in AMBER: 
- leaprc.protein.ff14SB for the protein
- leaprc.gaff2 for the ligand molecule
- leaprc.water.tip3p for the water molecules.

~~~
# Load force field parameters
source leaprc.protein.ff14SB
source leaprc.gaff2
source leaprc.water.tip3p
~~~
{: .source}

Now we have to include the following units: protein, ligand and the water molecules in ```tLeap```. We are going to start with the ligand using the two files we have created before: **GWS.frcmod** and **GWS.mol2** (mol2 file already includes the coordinates of the ligand). After, we load the coordinates for the protein and the water molecules with the ```loadPDB``` command.

~~~
# Load extraparameters for the ligand
loadamberparams GWS.frcmod

# Load protein, ligand and water molecules
protein = loadPDB protein4amber.pdb
GWS = loadmol2 GWS.mol2
waters = loadPDB waters.pdb
~~~
{: .source} 

From the very beginning of this series, we have splitted the system into these three pieces, now we join them again with the ```combine``` command. 

> ## TIP
> 
> It is good practice to check the resulting structure of this combination step (```savePDB``` command to save a PDB file) and to perform a sanity check on the unit we have created with ```check``` command: it determines the charge of the system, close contacts (it is normal to have them after adding hydrogen atoms) and checks if there are any missing parameters. 
{: .callout}

~~~
# Build system
system = combine {GWS protein waters}
savepdb system system.dry.pdb

check system
~~~
{: .source} 

If the unit is OK, we can continue! Now we need to solvate it and add counter ions to neutralise the system. 

To solvate the system, we can use 2 different commands: ```solvateOct``` and ```solvateBox```. These commands differ in the shape of the resulting simulation box: the first command creates an octaedric solvation box and the second a cubic solvation box. Since octaedric boxes are smaller and compatible with globular proteins, we are going to use ```solvateOct```. 

Last but not least, we need to neutralise the system by adding counterions. It is easy and simple to use the ```addions2``` command like this: ```addions2 system Na+ 0```.

~~~
# Solvate
solvateOct system TIP3PBOX 12 iso

# Neutralise
addions2 system Cl- 0
addions2 system Na+ 0
~~~
{: .source} 

Now we can proceed to save our topology and coordinate files with ```saveamberparm system system.parm7 system.rst7```. Before doing anything else, Leap checks that the unit it is OK and then proceeds to build the topology and write the coordinates. Finally it prints a summary of the ends of the system molecules. We have a SER in the N terminal end and THR on the C terminal end of the protein, a ligand named GWS and 20097 water molecules. 

~~~
# Save AMBER input files
savePDB system system.pdb
saveAmberParm system system.parm7 system.rst7

quit
~~~
{: .source}

To execute it:

~~~
tleap -f leapin
~~~
{: .language-bash}

You can find more information about Leap usage in the [AMBER documentation](https://ambermd.org/doc12/Amber20.pdf). Other Leap related FAQ can be found here: http://ambermd.org/Questions/leap.html

