---
layout: lesson
title: Setup
---

## Accessing ARCHER:

~~~
ssh -XY user@login.archer.ac.uk
~~~
{: .language-bash}

**Software we will be using in this session:**
- [AmberTools](https://ambermd.org/AmberTools.php) (We will use the latest version of Ambertools. If you are using an older version, commands might run slightly different.)
- [propKa](http://propka.org)
- [OpenBabel](http://openbabel.org/wiki/Main_Page)
- [Pymol](https://sourceforge.net/projects/pymol/) or [VMD](https://www.ks.uiuc.edu/Research/vmd/) or another molecular visualisation tool of your preference. 
- [Marvin Sketch](https://chemaxon.com/products/marvin). 

To set up the proper environment you should run the following commands on ARCHER:

~~~
module load propka
module load amber-tools/20
module load openbabel
~~~
{: .language-bash}

We'll do our work in the `preMD` folder so make sure you change your working directory to it with:

~~~
$ cd
$ cd preMD
~~~
{: .language-bash}

